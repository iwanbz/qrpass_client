import appConfig from '../app_config'
import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase' 
import 'firebase/firestore'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    mainNavigationDrawer: false,

    user: null,
    authorization: '',
    locale: 'id',
    branch: {
      uid: '',
      city: '',
      name: '',
    },
    branchKey: ''
  },
  getters: {
    branch: state => state.branch,
    locale: (state) => state.locale,
    user (state) { return state.user },
    authorization (state) { return state.authorization },
    appConfig: () => appConfig,
    branchKey: state => state.branchKey

  },
  mutations: {
    setUser (state, user) {
      if (user) {
        state.user = {
          uid: user.uid,
          email: user.email,
          displayName: user.displayName
        }
      } else {
        state.user = null
      }
    },
    setBranch (state, branch) {
      state.branch = branch
    },
    setBranchKey (state, branchKey) {
      state.branchKey = branchKey
    }
  },
  actions: {
    async updateBranchKey ({commit}, branchKey) {
      commit('setBranchKey', branchKey)
      localStorage.setItem('branchkey', branchKey)
      const db = firebase.firestore();
      db.collection('branches')
        .doc(branchKey)
        .get()
        .then(doc => {
          commit('setBranch', doc.data())
          localStorage.setItem('branch', JSON.stringify(doc.data()))
          return Promise.resolve(true) 
        })
        .catch(err => {
          return Promise.reject(err)
        })
    },

    loadBranchKey ({commit}) {
      if (localStorage.getItem('branchKey')) {
        commit('setBranchKey', localStorage.getItem('branchKey'))
        commit('setBranch', JSON.parse(localStorage.getItem('branch')))
      }
    },

    async signin ({ commit }, {email, password}) {
      return firebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => {
          commit('setUser', user)
          return true
        })
        .catch(err => {
          throw err
        })
    },
    reSignin ({commit}, user) {
      commit('setUser', user)
    },
    signout ({ commit }) {
      firebase.auth().signOut()
        .then(() => {
          commit('setUser', null)
        })
        .catch(err => {
          console.log(err)
        })
    },
    async signinGoogle ({ getters }) {
      firebase.auth().languageCode = getters.locale
      var provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().signInWithRedirect(provider)
        .then(() => {
          return true
        })
        .catch(err => {
          return Promise.reject(err)
        })
    },
    async signinFacebook ({ getters }) {
      firebase.auth().languageCode = getters.locale
      var provider = new firebase.auth.FacebookAuthProvider();
      firebase.auth().signInWithRedirect(provider)
        .then(() => {
          return true
        })
        .catch(err => {
          return Promise.reject(err)
        })
    },
    loadbranch ({commit}) {
      if (localStorage.getItem('branch')) {
        commit('setBranch', JSON.parse(localStorage.getItem('branch')))
      }
    } 

  },
  modules: {
  }
})
