import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store'
import router from './router'
import firebase from 'firebase'
import 'firebase/firestore'
import { firestorePlugin } from 'vuefire'
import firebaseAppConfig from './firebase-app-config.js'
Vue.config.productionTip = false

const firebaseApp = firebase.initializeApp(firebaseAppConfig)

Vue.use(firestorePlugin)
const firestore = firebaseApp.firestore();
let app = '';
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      firestore () {
        return {
          orders: firestore.collection('orders')
        }
      },
      vuetify,
      store,
      router,
      render: h => h(App),
      created () {
        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            this.$store.dispatch('reSignin', user)
          }
        })
      }
    }).$mount('#app')    
  }
})
