/* eslint-disable no-unused-vars */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import firebase from 'firebase'
import 'firebase/firestore'
Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    redirect: '/signin'
  },
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import(/* webpackChunkName: "login" */ '@/views/SignUp.vue')
  },
  {
    path: '/signin',
    name: 'signin',
    component: () => import(/* webpackChunkName: "login" */ '@/components/Signin.vue')
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Dashboard.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Profile.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/orders',
    name: 'orders',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Orders.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/setpos',
    name: 'setpos',
    component: () => import(/* webpackChunkName: "login" */ '@/views/SetPOS.vue'),
    meta: {
      requiresAuth: true
    }
  },  
  {
    path: '/branches',
    name: 'branches',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Branches.vue'),
    meta: {
      requiresAuth: true
    }
  },  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// eslint-disable-next-line no-unused-vars
router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requiresAuth && !currentUser) {
    next('login');
  } else {
    next()
  }  
});

export default router
